module Day01 (part1, part2) where

import qualified Data.List as List


calcFuel :: Int -> Int
calcFuel mass = max 0 $ mass `quot` 3 - 2

calcFuel' :: Int -> Int
calcFuel' = List.sum
        . (List.takeWhile $ (/=) 0)
        . List.tail
        . List.iterate calcFuel

calcTotalFuel :: (Int -> Int) -> [String] -> Int
calcTotalFuel formula = List.sum . List.map (formula . read)

part1 :: [String] -> Int
part1 = calcTotalFuel calcFuel

part2 :: [String] -> Int
part2 = calcTotalFuel calcFuel'
