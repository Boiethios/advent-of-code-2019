module Main where

import Control.Monad

import qualified Day01

main :: IO ()
main = do
    lines <- liftM lines $ readFile "input"
    print $ Day01.part1 lines
    print $ Day01.part2 lines
